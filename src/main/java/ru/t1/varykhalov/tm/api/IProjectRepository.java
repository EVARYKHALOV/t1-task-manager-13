package ru.t1.varykhalov.tm.api;

import ru.t1.varykhalov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String Id);

    Project removeByIndex(Integer index);

    Integer getSize();
}